---
title: Hello Docusaurus.io
author: Dennis Marinho
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

<!--truncate-->

## Agora sim!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

```
drwxrwxr-x  5 dennis dennis 4096 Nov 13 23:00 ./
drwxr-xr-x 55 dennis dennis 4096 Nov 15 22:41 ../
-rw-rw-r--  1 dennis dennis  497 Nov 13 22:29 docker-compose.yml
-rw-rw-r--  1 dennis dennis  148 Nov 13 22:29 Dockerfile
-rw-rw-r--  1 dennis dennis   21 Nov 13 22:29 .dockerignore
drwxrwxr-x  2 dennis dennis 4096 Nov 15 23:14 docs/
drwxrwxr-x  8 dennis dennis 4096 Nov 15 23:24 .git/
-rw-rw-r--  1 dennis dennis  165 Nov 13 22:29 .gitignore
-rw-rw-r--  1 dennis dennis  284 Nov 13 23:36 .gitlab-ci.yml
drwxrwxr-x  8 dennis dennis 4096 Nov 13 22:31 website/

```